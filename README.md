Redstu Console
=====
A react.js based application for interacting with the redstu engine

## Running the app

1 - Development mode: `npm start`

2 - Running  tests: `npm test`

3 - Building for production: `npm run build`

## Todo
* Settings - Update server address
* Settings - Update access key
* Signals - Add JSON editor  - https://github.com/AndrewRedican/react-json-editor-ajrm
* First setup - Configure server and access key if needed
* UI - Reposition the modal dialog to be more close to the top
* Unit tests
* Integration tests
* Browser tests