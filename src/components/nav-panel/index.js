import React, {useEffect, useState} from "react";
import {Link, useLocation} from 'react-router-dom'
import logo from '../../assets/logo.png';
import ServerSettings from "./server-settings";

function NavPanel() {

    const location = useLocation();
    const [selected, setSelected] = useState('');

    useEffect(() => {
        setSelected(location.pathname)
    }, [location.pathname])

    return (
        <div className="nav-panel">
            <div className="logo">
                <img src={logo} alt="redtsu logo"/>
            </div>
            <div className="menu">
                <ul>
                    <li className={selected === '/' ? 'active' : ''}><Link to="/">State</Link></li>
                    <li className={selected === '/signals' ? 'active' : ''}><Link to="/signals">Signals</Link></li>
                    <li className={selected === '/responders' ? 'active' : ''}><Link to="/responders">Responders</Link>
                    </li>
                </ul>
            </div>
            <div className="settings">
                <ServerSettings/>
            </div>
        </div>
    )
}

export default NavPanel;