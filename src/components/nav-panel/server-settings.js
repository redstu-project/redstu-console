import React, {useEffect, useState} from "react";
import {BiCog} from "react-icons/bi";
import Rodal from "rodal";
import projectApi from "../../adapters/project-api";

function ServerSettings() {

    const [modalVisible, setModalVisible] = useState(false);

    const [project, setProject] = useState([])

    const loadProjectData = () => {
        projectApi.getAll().then(result => {
            setProject(result);
        })
    }

    useEffect(() => {
        loadProjectData();
    }, [])

    return (
        <div className="server-settings">
            <button className="btn-nav" onClick={() => setModalVisible(true)}><BiCog/></button>

            <Rodal enterAnimation="slideDown" leaveAnimation="slideUp" visible={modalVisible}
                   height={360} onClose={() => setModalVisible(false)}>
                <div className="modal-header">
                    <h3>Settings</h3>
                </div>

                <div className="modal-body">
                    <form>
                        <input className="input-style" type="text" name="server" placeholder="Server"/>
                        <input className="input-style" type="text" name="accessKey" placeholder="Access Key"/>
                    </form>

                    <h3 className="h3 space-title">Project info</h3>

                    <div className="row">
                        <div className="desc">
                            <p className="label">Project</p>
                            <p className="data">{project.name}</p>
                        </div>

                        <div className="desc">
                            <p className="label">Engine version</p>
                            <p className="data">{project.engine}</p>
                        </div>
                    </div>


                    <div className="desc">
                        <p className="label">Author name</p>
                        <p className="data">{project.author.name}</p>
                    </div>

                    <div className="desc">
                        <p className="label">Author email</p>
                        <p className="data">{project.author.email}</p>
                    </div>
                </div>

            </Rodal>

        </div>
    )
}

export default ServerSettings;