import React from "react";

function Container({children, className}) {
    return (
        <div className={`container-body ${className}`}>
            {children}
        </div>
    )
}

export default Container;