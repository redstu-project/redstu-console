import React from 'react';
import ReactDOM from 'react-dom';
import 'rodal/lib/rodal.css';
import './styles/global.css';
import reportWebVitals from './reportWebVitals';
import {HashRouter as Router, Route, Switch} from "react-router-dom";
import NavPanel from "./components/nav-panel";
import State from "./pages/state";
import Signals from "./pages/signals";
import Responders from "./pages/responders";

ReactDOM.render(
    <React.StrictMode>
        <Router>
            <div className="app-container">
                <div className="nav-container">
                    <NavPanel/>
                </div>
                <div className="content-container">
                    <Switch>
                        <Route path="/" component={State} exact={true}/>
                        <Route path="/signals" component={Signals}/>
                        <Route path="/responders" component={Responders}/>
                    </Switch>
                </div>
            </div>
        </Router>
    </React.StrictMode>,
    document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
