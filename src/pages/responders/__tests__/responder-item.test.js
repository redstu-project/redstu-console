import React from "react";
import ResponderItem from "../responder-item";
import {shallow} from "enzyme";

it("renders without crashing", () => {
    const wrapper = shallow(<ResponderItem action="GET" name="/endpoint"/>);

    expect(wrapper.contains(<p className="type get">GET</p>)).toBe(true);
    expect(wrapper.contains(<p className="desc">/endpoint</p>)).toBe(true);
})