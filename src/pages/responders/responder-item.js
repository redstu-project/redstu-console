import React from "react";

function ResponderItem({action, name}) {
    return (
        <div className="endpointItem">
            <p className={`type ${action.toLowerCase()}`}>{action}</p>
            <p className="desc">{name}</p>
        </div>
    )
}

export default ResponderItem;