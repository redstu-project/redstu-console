import React, {useEffect, useState} from "react";
import serverApi from "../../adapters/server-api";
import ResponderItem from "./responder-item";
import Container from "../../components/container";

export default function Responders() {

    const [responders, setResponders] = useState([])

    const loadResponders = () => {
        serverApi.getResponders().then(result => {
            setResponders(result);
        })
    }

    useEffect(() => {
        loadResponders();
    }, [])

    return (
        <Container>
            {responders.map(item => <ResponderItem action={item.verb}
                                                   name={item.path}
                                                   key={item.verb + '-' + item.path}/>)}
        </Container>
    )

}