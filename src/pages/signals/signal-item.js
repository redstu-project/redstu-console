import React from "react";
import SignalEmit from "./signal-emit";

function SignalItem({signal}) {

    return (
        <div className="signalItem">
            <div className="signal-name">
                <p className="signal-title">{signal.name}</p>
                <p className="desc-signal">{signal.description}</p>
            </div>
            <SignalEmit signal={signal}/>
        </div>
    )
}

export default SignalItem;