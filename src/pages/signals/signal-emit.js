import React, {useState} from "react";
import Rodal from "rodal";
import signalsApi from "../../adapters/signals-api";

function SignalEmit({signal}) {

    const [modalVisible, setModalVisible] = useState(false);
    const [signalBody, setSignalBody] = useState('');

    const emitSignal = (name, body) => {
        signalsApi.emit(name, body).then(() => {
            console.log('signal emitted')
            setSignalBody('')
        })
    }

    const handleEmitButtonClick = () => {
        if (signal.parameterized) {
            setModalVisible(true)
        } else {
            emitSignal(signal.name, {})
        }
    }

    const handleEmitModalButtonClick = () => {
        emitSignal(signal.name, {description: signalBody});
        setModalVisible(false);
    }

    function handleParameterChange(event) {
        setSignalBody(event.target.value);
    }

    return (
        <>
            <button className="btn btn-round btn-sm btn-ghost-grey"
                    onClick={() => handleEmitButtonClick()}>Emit
            </button>

            <Rodal enterAnimation="slideDown"
                   leaveAnimation="slideUp" visible={modalVisible}
                   height={220}
                   onClose={() => setModalVisible(false)}>

                <div className="modal-header">
                    <h3>Emit signal: <span className="red">{signal.name}</span></h3>
                </div>

                <div className="modal-body">
                    <label>Parameter</label>
                    <textarea value={signalBody} onChange={handleParameterChange} rows="6"/>
                </div>

                <div className="modal-footer">
                    <button className="btn btn-round btn-sm-1 btn-ghost-grey"
                            onClick={() => handleEmitModalButtonClick()}>Emit
                    </button>
                    <button className="btn btn-round btn-sm-1 btn-outline-red ml-10"
                            onClick={() => setModalVisible(false)}>Cancel
                    </button>
                </div>
            </Rodal>
        </>
    )
}

export default SignalEmit;