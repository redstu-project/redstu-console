import Container from "../../components/container";
import React, {useEffect, useState} from "react";
import signalsApi from "../../adapters/signals-api";
import SignalItem from "./signal-item";

export default function Signals() {

    const [signalList, setSignalList] = useState([])

    const loadSignals = () => {
        signalsApi.getAll().then(result => {
            setSignalList(result);
        })
    }

    useEffect(() => {
        loadSignals();
    }, [])

    return (
        <Container>
            {signalList.map((item, i) => <SignalItem signal={item} key={i}/>)}
        </Container>
    )
}