import Container from "../../components/container";
import React, {useEffect, useState} from "react";
import ReactJson from "react-json-view";
import stateApi from "../../adapters/state-api";
import {BiCloudDownload, BiCloudUpload} from "react-icons/bi";

export default function State() {

    const [objectTree, setObjectTree] = useState({})

    const loadState = () => {
        stateApi.getAll().then(result => {
            setObjectTree(result);
        })
    }

    const saveNewState = obj => {
        stateApi.put(obj).then(() => {
            setObjectTree(obj);
        })
    }

    const handleJsonEdit = data => {
        console.log(data);
    }

    const handleUpload = () => {
        let uploadElement = document.getElementById('contentFile');

        uploadElement.onchange = event => {
            let reader = new FileReader();
            reader.onload = readEvent => {
                let obj = JSON.parse(readEvent.target.result);
                saveNewState(obj)
                uploadElement.value = "";
            };
            reader.readAsText(event.target.files[0]);
        }

        uploadElement.click();
    }

    const handleDownload = () => {
        let dataStr = "data:text/json;charset=utf-8," + encodeURIComponent(JSON.stringify(objectTree));
        let downloadAnchorNode = document.createElement('a');
        downloadAnchorNode.setAttribute("href", dataStr);
        downloadAnchorNode.setAttribute("download", "state-object.json");
        document.body.appendChild(downloadAnchorNode); // required for firefox
        downloadAnchorNode.click();
        downloadAnchorNode.remove();
    }

    useEffect(() => {
        loadState();
    }, [])

    return (
        <Container className="state-container">
            <div className="content-area">
                <ReactJson
                    collapsed={3}
                    src={objectTree}
                    onEdit={edit => saveNewState(edit.updated_src)}
                    onAdd={add => saveNewState(add.updated_src)}
                    onDelete={del => saveNewState(del.updated_src)}
                />
            </div>
            <div className="action-area">
                <button className="btn btn-round btn-md btn-ghost-grey" onClick={() => handleUpload()}>
                    <BiCloudUpload/>
                </button>

                <input id="contentFile" type="file" accept="application/json"/>

                <button className="btn btn-round btn-md btn-ghost-grey" onClick={() => handleDownload()}>
                    <BiCloudDownload/>
                </button>
            </div>
        </Container>
    )
}