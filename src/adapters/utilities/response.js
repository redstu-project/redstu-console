export function handleResponse(response) {
    if (response.results) {
        return response.results;
    }

    if (response.data) {
        return response.data;
    }

    return response;
}

export function handleError(error) {
    if (error.message === 'Network Error' || error.response.status === 404
    ) {
        alert('Ups. Houve uma falha na comunicação com o servidor. Verifique sua conexão com a internet.');
    }
    //throw error;
}