import {ApiCore} from "./utilities/core";

const projectApi = new ApiCore({
    getAll: true,
    url: 'project',
});


export default projectApi;