import {ApiCore} from "./utilities/core";
import apiProvider from "./utilities/provider";

const signalsApi = new ApiCore({
    getAll: true,
    url: 'signals',
});

signalsApi.emit = (name, body) => {
    return apiProvider.post(`${signalsApi.url}/${name}`, body)
}

export default signalsApi;