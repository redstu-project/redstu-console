import {ApiCore} from "./utilities/core";

const stateApi = new ApiCore({
    getAll: true,
    put: true,
    url: 'state',
});


export default stateApi;