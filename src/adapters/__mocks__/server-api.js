const serverApi = {};

serverApi.getSecurityScheme = () => {
    return {}
}

serverApi.getResponders = () => {
    return [
        {verb: 'GET', path: '/endpoint1'},
        {verb: 'POST', path: '/endpoint2'},
        {verb: 'PUT', path: '/endpoint3'}
    ]
}

export default serverApi;