import {ApiCore} from "./utilities/core";
import apiProvider from "./utilities/provider";

const serverApi = new ApiCore({
    url: 'servers',
});

serverApi.getSecurityScheme = () => {
    return apiProvider.getAll(`${serverApi.url}/cockpit/security`)
}

serverApi.getResponders = () => {
    return apiProvider.getAll(`${serverApi.url}/mock/responders`)
}

export default serverApi;